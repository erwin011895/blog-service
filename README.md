# Blog API

## Run on local env

1. Run Postgres and Create 1 Database
2. Run SQL queries at `internal/migration/000001_init_tables.sql`
3. Fill in DB configurations & server port in `config.json`
4. Run `go run main.go` (if you have [Make](https://www.gnu.org/software/make/) installed, you can run `make run`)

## Postman

There is a Postman collection file at root: `blog.postman_collection.json`.

You can import the collection for local testing.