package config

import (
	"encoding/json"
	"os"
	"path/filepath"
)

var singletonConf *Configuration

type Configuration struct {
	Server   Server   `json:"server"`
	Postgres Postgres `json:"postgres"`
}

type Server struct {
	Port string `json:"port"`
}

type Postgres struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Database string `json:"database"`
}

func init() {
	if singletonConf != nil {
		return
	}

	basePath, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	configContent, err := os.ReadFile(filepath.Join(basePath, "config.json"))
	if err != nil {
		panic(err)
	}

	singletonConf = new(Configuration)
	err = json.Unmarshal(configContent, &singletonConf)
	if err != nil {
		panic(err)
	}
}

func Get() *Configuration {
	return singletonConf
}
