package datastore

import "time"

type Post struct {
	Id          int
	Title       string
	Content     string
	Status      string
	PublishDate *time.Time
	TagIds      []int
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

type Tag struct {
	Id    int
	Label string
}
