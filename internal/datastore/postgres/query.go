package postgres

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/erwin011895/blog-service/internal/datastore"
)

func InsertPost(ctx context.Context, tx *sql.Tx, post datastore.Post) error {
	var err error
	q := `INSERT INTO posts (title, content, tag_ids, status, created_at, updated_at)
		VALUES ($1, $2, $3, 'draft', NOW(), NOW())`

	_, err = tx.ExecContext(ctx, q, post.Title, post.Content, pq.Array(post.TagIds))
	return err
}

func GetPost(ctx context.Context, id uint64) (*datastore.Post, error) {
	var err error

	db := GetDB()

	q := `SELECT id, title, content, tag_ids, status, publish_date FROM posts
		WHERE deleted_at is NULL AND id = $1`

	row := db.QueryRowContext(ctx, q, id)

	err = row.Err()
	if err != nil {
		return nil, row.Err()
	}

	tempTagIds := []sql.NullInt64{}
	post := datastore.Post{}
	err = row.Scan(
		&post.Id,
		&post.Title,
		&post.Content,
		pq.Array(&tempTagIds),
		&post.Status,
		&post.PublishDate,
	)
	if err != nil {
		return nil, err
	}

	for _, id := range tempTagIds {
		post.TagIds = append(post.TagIds, int(id.Int64))
	}

	return &post, err
}

func GetPostsByTag(ctx context.Context, tag string) ([]*datastore.Post, error) {
	result := []*datastore.Post{}
	var err error

	db := GetDB()

	tags, err := GetTags(ctx, nil, GetTagsParam{
		Labels: []string{tag},
	})
	if err != nil {
		return nil, err
	}

	if len(tags) == 0 {
		return nil, nil
	}

	tagId := tags[0].Id

	q := `SELECT id, title, content, tag_ids, status, publish_date FROM posts
		WHERE deleted_at is NULL AND $1 = ANY(tag_ids)`

	rows, err := db.QueryContext(ctx, q, tagId)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Err()
		if err != nil {
			return nil, err
		}

		tempTagIds := []sql.NullInt64{}
		post := datastore.Post{}
		rows.Scan(
			&post.Id,
			&post.Title,
			&post.Content,
			pq.Array(&tempTagIds),
			&post.Status,
			&post.PublishDate,
		)
		for _, id := range tempTagIds {
			post.TagIds = append(post.TagIds, int(id.Int64))
		}

		result = append(result, &post)
	}

	return result, err
}

func UpdatePost(ctx context.Context, tx *sql.Tx, post datastore.Post) error {
	var err error
	q := `UPDATE posts SET title = $1, content = $2, tag_ids = $3, status = $4, publish_date = $5, deleted_at = $6, updated_at = NOW()
		WHERE id = $7`

	_, err = tx.ExecContext(ctx, q,
		post.Title,
		post.Content,
		pq.Array(post.TagIds),
		post.Status,
		post.PublishDate,
		post.DeletedAt,
		post.Id,
	)
	return err
}

func InsertTags(ctx context.Context, tx *sql.Tx, tags []*datastore.Tag) error {
	var err error
	stmt, err := tx.PrepareContext(ctx, "INSERT INTO tags (label, created_at, updated_at) VALUES ($1, NOW(), NOW())")
	if err != nil {
		return err
	}

	for _, t := range tags {
		_, err = stmt.ExecContext(ctx, t.Label)
		if err != nil {
			return err
		}
	}

	return err
}

type GetTagsParam struct {
	Ids    []int
	Labels []string
}

func GetTags(ctx context.Context, tx *sql.Tx, param GetTagsParam) ([]*datastore.Tag, error) {
	result := []*datastore.Tag{}
	var err error

	if tx == nil {
		tx, err = GetDB().BeginTx(ctx, nil)
		if err != nil {
			return nil, err
		}
	}

	var rows *sql.Rows
	if len(param.Ids) > 0 {
		rows, err = tx.QueryContext(ctx, "SELECT id, label FROM tags WHERE id = ANY($1)", pq.Array(param.Ids))
		if err != nil {
			return nil, err
		}
	} else {
		rows, err = tx.QueryContext(ctx, "SELECT id, label FROM tags WHERE label = ANY($1::text[])", pq.Array(param.Labels))
		if err != nil {
			return nil, err
		}
	}

	for rows.Next() {
		tag := datastore.Tag{}
		rows.Scan(
			&tag.Id,
			&tag.Label,
		)

		result = append(result, &tag)
	}

	return result, err
}
