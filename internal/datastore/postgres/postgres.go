package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	// github.com/lib/pq
	"gitlab.com/erwin011895/blog-service/internal/config"
)

var singletonPostgres *sql.DB

func init() {
	if singletonPostgres != nil {
		return
	}

	conf := config.Get()

	connectionStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		conf.Postgres.Host,
		conf.Postgres.Port,
		conf.Postgres.User,
		conf.Postgres.Password,
		conf.Postgres.Database,
	)

	db, err := sql.Open("postgres", connectionStr)
	if err != nil {
		panic(err)
	}

	db.SetMaxOpenConns(10)

	singletonPostgres = db
}

func GetDB() *sql.DB {
	return singletonPostgres
}
