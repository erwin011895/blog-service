-- CREATE DATABASE blog_service;

CREATE TABLE IF NOT EXISTS posts ( 
    id BIGSERIAL NOT NULL,
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    "status" VARCHAR(10) NOT NULL, -- draft / publish
    publish_date DATE,
    tag_ids INTEGER ARRAY NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP,
    PRIMARY KEY (id)
);
CREATE INDEX IF NOT EXISTS "posts_tag_ids" ON posts USING gin(tag_ids); -- general inverted index

CREATE TABLE IF NOT EXISTS tags ( 
    id bigserial NOT NULL,
    label VARCHAR(100) UNIQUE NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
);
