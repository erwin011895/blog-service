package routes

import (
	"net/http"

	"gitlab.com/erwin011895/blog-service/internal/handler"
)

func Set(mux *http.ServeMux) {
	mux.HandleFunc("/api/posts", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			handler.GetPostsByTag(w, r)
		case http.MethodPost:
			handler.CreatePost(w, r)
		default:
			handler.ResponseNotFound(w, "")
		}
	})

	mux.HandleFunc("/api/posts/{post_id}", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			handler.GetPostById(w, r)
		case http.MethodPut:
			handler.UpdatePost(w, r)
		case http.MethodDelete:
			handler.DeletePost(w, r)
		default:
			handler.ResponseNotFound(w, "")
		}
	})
}
