package handler

import (
	"context"
	"database/sql"
	"encoding/json"
	"net/http"

	"gitlab.com/erwin011895/blog-service/internal/datastore"
	"gitlab.com/erwin011895/blog-service/internal/datastore/postgres"
)

type CreatePostPayload struct {
	Title   string   `json:"title"`
	Content string   `json:"content"`
	Tags    []string `json:"tags"`
}

func CreatePost(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()
	select {
	case <-ctx.Done():
		ResponseInternalServerError(w, err, "")
	default:
	}

	decoder := json.NewDecoder(r.Body)
	payload := CreatePostPayload{}
	if err := decoder.Decode(&payload); err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	if payload.Title == "" {
		ResponseJSON(w, http.StatusBadRequest, map[string]string{
			"title": "Title is required",
		})
		return
	}

	if payload.Content == "" {
		ResponseJSON(w, http.StatusBadRequest, map[string]string{
			"content": "Content is required",
		})
		return
	}

	db := postgres.GetDB()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	tags, err := postgres.GetTags(ctx, tx, postgres.GetTagsParam{
		Labels: payload.Tags,
	})
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	if len(tags) < len(payload.Tags) {
		tags, err = syncTagsData(ctx, tx, tags, payload.Tags)
		if err != nil {
			ResponseInternalServerError(w, err, "")
			return
		}
	}

	// insert post with tags
	tagIds := []int{}
	for _, t := range tags {
		tagIds = append(tagIds, t.Id)
	}

	err = postgres.InsertPost(ctx, tx, datastore.Post{
		Title:   payload.Title,
		Content: payload.Content,
		TagIds:  tagIds,
	})
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	err = tx.Commit()
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	ResponseJSONSuccess(w, http.StatusOK)
}

func syncTagsData(ctx context.Context, tx *sql.Tx, tagsDb []*datastore.Tag, tagsPayload []string) ([]*datastore.Tag, error) {
	// find new tags to insert
	mapTagFromPayload := map[string]bool{}
	for _, t := range tagsPayload {
		mapTagFromPayload[t] = true
	}

	for _, t := range tagsDb {
		delete(mapTagFromPayload, t.Label)
	}

	tagsToInsert := []*datastore.Tag{}
	for tagLabel := range mapTagFromPayload {
		tagsToInsert = append(tagsToInsert, &datastore.Tag{
			Label: tagLabel,
		})
	}

	// do insert and re-get tags data
	err := postgres.InsertTags(ctx, tx, tagsToInsert)
	if err != nil {
		return nil, err
	}

	tagsDb, err = postgres.GetTags(ctx, tx, postgres.GetTagsParam{
		Labels: tagsPayload,
	})
	if err != nil {
		return nil, err
	}

	return tagsDb, nil
}
