package handler

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/erwin011895/blog-service/internal/datastore/postgres"
)

type UpdatePostPayload struct {
	Title   string   `json:"title"`
	Content string   `json:"content"`
	Tags    []string `json:"tags"`
	Status  string   `json:"status"`
}

func UpdatePost(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()
	select {
	case <-ctx.Done():
		ResponseInternalServerError(w, err, "")
	default:
	}

	postIdStr := r.PathValue("post_id")
	postId, err := strconv.ParseUint(postIdStr, 10, 64)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	decoder := json.NewDecoder(r.Body)
	payload := UpdatePostPayload{}
	if err := decoder.Decode(&payload); err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	if payload.Title == "" {
		ResponseJSON(w, http.StatusBadRequest, map[string]string{
			"title": "Title is required",
		})
		return
	}

	if payload.Content == "" {
		ResponseJSON(w, http.StatusBadRequest, map[string]string{
			"content": "Content is required",
		})
		return
	}

	if payload.Status != "draft" && payload.Status != "publish" {
		ResponseJSON(w, http.StatusBadRequest, map[string]string{
			"status": "status must be either draft or publish",
		})
		return
	}

	db := postgres.GetDB()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	post, err := postgres.GetPost(ctx, postId)
	if err != nil {
		if err == sql.ErrNoRows {
			ResponseNotFound(w, "post not found")
			return
		}

		ResponseInternalServerError(w, err, "")
		return
	}

	post.Title = payload.Title
	post.Content = payload.Content
	post.Status = payload.Status
	if post.Status == "publish" {
		now := time.Now()
		post.PublishDate = &now
	}

	tags, err := postgres.GetTags(ctx, tx, postgres.GetTagsParam{
		Labels: payload.Tags,
	})
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	// sync tags data
	if len(tags) < len(payload.Tags) {
		if len(tags) < len(payload.Tags) {
			tags, err = syncTagsData(ctx, tx, tags, payload.Tags)
			if err != nil {
				ResponseInternalServerError(w, err, "")
				return
			}
		}
	}

	// update post
	tagIds := []int{}
	for _, t := range tags {
		tagIds = append(tagIds, t.Id)
	}
	post.TagIds = tagIds

	err = postgres.UpdatePost(ctx, tx, *post)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	err = tx.Commit()
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	ResponseJSONSuccess(w, http.StatusOK)
}
