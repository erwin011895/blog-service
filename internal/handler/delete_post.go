package handler

import (
	"database/sql"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/erwin011895/blog-service/internal/datastore/postgres"
)

func DeletePost(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()
	select {
	case <-ctx.Done():
		ResponseInternalServerError(w, err, "")
	default:
	}

	postIdStr := r.PathValue("post_id")
	postId, err := strconv.ParseUint(postIdStr, 10, 64)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	db := postgres.GetDB()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	post, err := postgres.GetPost(ctx, postId)
	if err != nil {
		if err == sql.ErrNoRows {
			ResponseNotFound(w, "post not found")
			return
		}

		ResponseInternalServerError(w, err, "")
		return
	}

	// delete post via soft delete
	now := time.Now()
	post.DeletedAt = &now

	err = postgres.UpdatePost(ctx, tx, *post)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	err = tx.Commit()
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	ResponseJSONSuccess(w, http.StatusOK)
}
