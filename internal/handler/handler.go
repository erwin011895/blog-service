package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"runtime/debug"
)

func ResponseJSON(w http.ResponseWriter, statusCode int, output interface{}) {
	res, err := json.Marshal(output)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(res)
}

func ResponseJSONSuccess(w http.ResponseWriter, statusCode int) {
	res, err := json.Marshal(map[string]string{
		"message": "success",
	})
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(res)
}

func ResponseNotFound(w http.ResponseWriter, message string) {
	w.WriteHeader(http.StatusNotFound)
	if message == "" {
		w.Write([]byte("404 not found!"))
	} else {
		w.Write([]byte(message))
	}
}

func ResponseInternalServerError(w http.ResponseWriter, err error, message string) {
	if err != nil {
		debug.PrintStack()
		log.Println(err)
	}

	w.WriteHeader(http.StatusInternalServerError)
	if message == "" {
		w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
	} else {
		w.Write([]byte(message))
	}
}
