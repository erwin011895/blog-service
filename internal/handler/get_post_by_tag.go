package handler

import (
	"database/sql"
	"net/http"

	"gitlab.com/erwin011895/blog-service/internal/datastore/postgres"
)

type GetPostsByTagResponse struct {
	Title   string   `json:"title"`
	Content string   `json:"content"`
	Tags    []string `json:"tags"`
}

func GetPostsByTag(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()
	select {
	case <-ctx.Done():
		ResponseInternalServerError(w, err, "")
	default:
	}

	tag := r.URL.Query().Get("tag")

	posts, err := postgres.GetPostsByTag(ctx, tag)
	if err != nil {
		if err == sql.ErrNoRows {
			ResponseJSON(w, http.StatusOK, posts)
			return
		}

		ResponseInternalServerError(w, err, "")
		return
	}

	tagIds := []int{}
	for _, p := range posts {
		tagIds = append(tagIds, p.TagIds...)
	}

	tags, err := postgres.GetTags(ctx, nil, postgres.GetTagsParam{
		Ids: tagIds,
	})
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	tagLabelMap := map[int]string{}
	for _, t := range tags {
		tagLabelMap[t.Id] = t.Label
	}

	resp := []GetPostsByTagResponse{}
	for _, p := range posts {
		tagLabels := []string{}
		for _, tagId := range p.TagIds {
			tagLabels = append(tagLabels, tagLabelMap[tagId])
		}
		resp = append(resp, GetPostsByTagResponse{
			Title:   p.Title,
			Content: p.Content,
			Tags:    tagLabels,
		})
	}
	ResponseJSON(w, http.StatusOK, resp)
}
