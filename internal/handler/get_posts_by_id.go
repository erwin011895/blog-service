package handler

import (
	"database/sql"
	"net/http"
	"strconv"

	"gitlab.com/erwin011895/blog-service/internal/datastore/postgres"
)

type GetPostResponse struct {
	Title   string   `json:"title"`
	Content string   `json:"content"`
	Tags    []string `json:"tags"`
}

func GetPostById(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()
	select {
	case <-ctx.Done():
		ResponseInternalServerError(w, err, "")
	default:
	}

	postIdStr := r.PathValue("post_id")
	postId, err := strconv.ParseUint(postIdStr, 10, 64)
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	post, err := postgres.GetPost(ctx, postId)
	if err != nil {
		if err == sql.ErrNoRows {
			ResponseNotFound(w, "post not found")
			return
		}

		ResponseInternalServerError(w, err, "")
		return
	}

	tags, err := postgres.GetTags(ctx, nil, postgres.GetTagsParam{
		Ids: post.TagIds,
	})
	if err != nil {
		ResponseInternalServerError(w, err, "")
		return
	}

	tagLabels := []string{}
	for _, t := range tags {
		tagLabels = append(tagLabels, t.Label)
	}

	ResponseJSON(w, http.StatusOK, GetPostResponse{
		Title:   post.Title,
		Content: post.Content,
		Tags:    tagLabels,
	})
}
