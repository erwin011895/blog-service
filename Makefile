SHELL := /bin/bash

init:
	go mod init

tidy:
	go mod tidy

run:
	go run main.go

build:
	go build
