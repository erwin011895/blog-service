package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/erwin011895/blog-service/internal/config"
	"gitlab.com/erwin011895/blog-service/internal/routes"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	mux := http.DefaultServeMux
	routes.Set(mux)

	var handler http.Handler = mux

	server := new(http.Server)
	server.Addr = ":" + config.Get().Server.Port
	server.Handler = handler

	fmt.Println("server started at localhost" + server.Addr)
	server.ListenAndServe()
}
